﻿using Discord.WebSocket;
using Discord;
using Discord.Commands;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace ARTEMIS
{
    public class Program
    {
        static void Main(string[] args) => new Program().Start().GetAwaiter().GetResult();

        private DiscordSocketClient client;
        private CommandService commands;
        private IServiceProvider service;

        public async Task Start()
        {
            client = new DiscordSocketClient();
            client.Log += Log;

            commands = new CommandService();
            await InstallCommands();

            service = ConfigureServices();
            service.GetRequiredService<CommandService>();

            await client.LoginAsync(TokenType.Bot, "");
            await client.StartAsync();
            client.Ready += ReadyEvent;

            await Task.Delay(-1);
        }

        private async Task InstallCommands()
        {
            client.MessageReceived += HandleCommand;
            await commands.AddModulesAsync(Assembly.GetEntryAssembly());
        }

        private Task Log(LogMessage message)
        {
            Console.WriteLine(message.ToString());
            return Task.CompletedTask;
        }

        private async Task HandleCommand(SocketMessage msg)
        {
            var message = msg as SocketUserMessage;

            if (message == null) return;

            int argPos = 0;

            if (!(message.HasCharPrefix('>', ref argPos) || message.HasMentionPrefix(client.CurrentUser, ref argPos))) return;

            var context = new CommandContext(client, message);
            var result = await commands.ExecuteAsync(context, argPos, service);

            if (!result.IsSuccess)
            {
                await message.Channel.SendMessageAsync(result.ErrorReason);
            }
        }

        private IServiceProvider ConfigureServices()
        {
            return new ServiceCollection()
                .AddSingleton(commands)
                .BuildServiceProvider();
        }

        private async Task ReadyEvent()
        {
            IApplication bot = await client.GetApplicationInfoAsync();

            // Update username to current name in app
            await client.CurrentUser.ModifyAsync(x =>
            {
                x.Username = bot.Name;
            });

            await client.SetGameAsync($">help");
        }
    }
}
